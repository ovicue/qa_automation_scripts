//Version 1.0.1

var fs = require('fs');
var builder = require('xmlbuilder');
var csv = require("fast-csv");


//GLOBAL VAR

var input = [];

 
//PARSE the CSV file

csv
 .fromPath("input.csv")
 .on("data", function(data){
     input.push(data);
 })
 .on("end", function(){
     console.log("done");


      //XML FILE GENERATOR

      xml = builder.create('?xml', {encoding: 'utf-8'})
            .ele('inputSet', {'description': '', 'name': 'Wayne1' })

      for(i=0; i<input.length; i++) {

        var j = 0;
        var name = input[i][j];
        var description = input[i][j+1];
        var orig_lat = input[i][j+2];
        var orig_lon = input[i][j+3];
		var waypoint1_lat = input[i][j+4];
        var waypoint1_lon = input[i][j+5];
		var waypoint2_lat = input[i][j+6];
        var waypoint2_lon = input[i][j+7];
        var dest_lat = input[i][j+8];
        var dest_lon = input[i][j+9];

        if (waypoint2_lat === undefined && dest_lat === undefined) {
            var dest_lat = input[i][j+4];
            var dest_lon = input[i][j+5];
            var waypoint1_lat = undefined;
            var waypoint1_lon = undefined;
        } else if (waypoint2_lat !== undefined && dest_lat === undefined) {
            var dest_lat = input[i][j+6];
            var dest_lon = input[i][j+7];
            var waypoint2_lat = undefined;
            var waypoint2_lon = undefined;
        } else {
            var dest_lat = input[i][j+8];
            var dest_lon = input[i][j+9];
        }


        if (waypoint1_lat === undefined && waypoint2_lat === undefined) {
            testcase = builder.create('testcase')
                .att({'description': description, 'name': name })
                    .ele('param',{'name': 'orig', 'type': 'java.lang.String', 'value': orig_lat + ',' + orig_lon})
                    .up()
                    .ele('param',{'name': 'dest', 'type': 'java.lang.String', 'value': dest_lat + ',' + dest_lon})
                    .up()
                    .ele('param',{'name': 'Routestyle', 'type': 'long', 'value': '1'})
                    .up()
                    .ele('param',{'name': 'Control', 'type': 'java.lang.String', 'value': 'FANVER=2|ROUTEVER=2|SINGLE_ROUTE=true'})
                    .up()
                    .ele('param',{'name': 'Country', 'type': 'java.lang.String', 'value': 'US'})
                    .up()
                    .ele('param',{'name': 'TrafficControl', 'type': 'java.lang.String', 'value': ''})
                    .up()

            xml.importDocument(testcase);
            
        } else if (waypoint1_lat !== undefined && waypoint2_lat === undefined) {
            testcase = builder.create('testcase')
                .att({'description': description, 'name': name })
                        .ele('param',{'name': 'orig', 'type': 'java.lang.String', 'value': orig_lat + ',' + orig_lon})
                        .up()
                        .ele('param',{'name': 'waypoint1', 'type': 'java.lang.String', 'value': waypoint1_lat + ',' + waypoint1_lon})
                        .up()
                        .ele('param',{'name': 'dest', 'type': 'java.lang.String', 'value': dest_lat + ',' + dest_lon})
                        .up()
                        .ele('param',{'name': 'Routestyle', 'type': 'long', 'value': '1'})
                        .up()
                        .ele('param',{'name': 'Control', 'type': 'java.lang.String', 'value': 'FANVER=2|ROUTEVER=2|SINGLE_ROUTE=true'})
                        .up()
                        .ele('param',{'name': 'Country', 'type': 'java.lang.String', 'value': 'US'})
                        .up()
                        .ele('param',{'name': 'TrafficControl', 'type': 'java.lang.String', 'value': ''})
                        .up()

            xml.importDocument(testcase);

        } else {
            testcase = builder.create('testcase')
                .att({'description': description, 'name': name })
                        .ele('param',{'name': 'orig', 'type': 'java.lang.String', 'value': orig_lat + ',' + orig_lon})
                        .up()
                        .ele('param',{'name': 'waypoint1', 'type': 'java.lang.String', 'value': waypoint1_lat + ',' + waypoint1_lon})
                        .up()
                        .ele('param',{'name': 'waypoint2', 'type': 'java.lang.String', 'value': waypoint2_lat + ',' + waypoint2_lon})
                        .up()
                        .ele('param',{'name': 'dest', 'type': 'java.lang.String', 'value': dest_lat + ',' + dest_lon})
                        .up()
                        .ele('param',{'name': 'Routestyle', 'type': 'long', 'value': '1'})
                        .up()
                        .ele('param',{'name': 'Control', 'type': 'java.lang.String', 'value': 'FANVER=2|ROUTEVER=2|SINGLE_ROUTE=true'})
                        .up()
                        .ele('param',{'name': 'Country', 'type': 'java.lang.String', 'value': 'US'})
                        .up()
                        .ele('param',{'name': 'TrafficControl', 'type': 'java.lang.String', 'value': ''})
                        .up()

            xml.importDocument(testcase);
        }
      }

      xml.doc().end({ pretty: true })

      fs.writeFileSync(
      __dirname + '/EU_slight' + '.xml',
      xml
      );
 });









